from django.apps import AppConfig


class AbrievConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'abriev'
